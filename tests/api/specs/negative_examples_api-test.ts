import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const auth = new AuthController();
const users = new UsersController();
const post = new PostsController()
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe("Negative tests", () => {

    let accessToken: string;
    let newUserId: number;
    let addComentData: object;


    before(`Register and get the token`, async () => {
        let response = await auth.register(0, "http://placeimg.com/940/480/animals", "skatchkova_liz@icloud.com", "OksanaDem8", "Lusia2021");
        checkStatusCode(response, 201);

        accessToken = response.body.token.accessToken.token;
        newUserId = response.body.user.id

    });


    it(`The user shouldn't be loged in with invalid credentialls`, async () => {
        let response = await auth.login("skatchkova_liz@icloud.com", "Lusia72021");
        checkStatusCode(response, 401);
        checkResponseTime(response,3000);
        
    });

    it(`The user shouldn't be loged in with empty password field`, async () => {
        let response = await auth.login("skatchkova_liz@icloud.com", "");
        checkStatusCode(response, 401);
        checkResponseTime(response,3000);
        
    });
    
        it(`3shouldn't add new comment to post with empty "postId" field`, async () => {
            addComentData = {
                    authorId: newUserId,
                    postId: "  ",
                    body: "tryjkopo[p;poiuytr"
            };
            let response = await post.createComentToPost(addComentData, accessToken);
            checkStatusCode(response, 400);
            checkResponseTime(response,3000);
        });
    
        
    
         after(`should delete user by id `, async () => {
        let response = await users.deleteUserById(newUserId, accessToken );
        checkStatusCode(response, 204);
        checkResponseTime(response,3000);
        
    });
})