import {
    checkResponseTime,
    checkStatusCode,
} from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';
const regist = new AuthController();

describe('Use test data set for register', () => {
    let invalidCredentialsDataSet = [
        { id: 0, avatar: 'http://placeimg.com/640/480/animals', email: 'alex.qa.test', userName: 'Yelyzaveta697', password: 'Karolusia234' },
        { id: 0, avatar: 'http://placeimg.com/640/480/animals', email: '', userName: 'yely$$$zaveta', password: 'Karolusia234' },
        { id: 0, avatar: 'http://placeimg.com/640/480/animals', email: 'alex.qa.test@gmail.com', userName: '', password: '56787654' },
        { id: 0, avatar: 'http://placeimg.com/640/480/animals', email: 'alex.qa.test@gmail.com', userName: 'Yelyzaveta697', password: '' },
     ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not be regisrered using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await regist.register(credentials.id, credentials.avatar, credentials.email, credentials.userName, credentials.password);
console.log(response.body)
            checkStatusCode(response, 400);
            checkResponseTime(response, 3000);
        });
    });
});

