import { expect } from "chai";
import { faker } from '@faker-js/faker';
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const auth = new AuthController();
const users = new UsersController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe("Happy path", () => {

    let accessToken: string;
    let userId: number;
    let userDataBeforeUpdate, userDataToUpdate;
    const randomEmail = faker.internet.email();
    const randomUserName = faker.internet.userName()
    const randonPassword = faker.internet.password()

    it(`The user should be registered with valid credentialls`, async () => {
        let response = await auth.register(0, "http://placeimg.com/640/480/animals", randomEmail, randomUserName, randonPassword);
       
        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;

        checkStatusCode(response, 201);
        checkResponseTime(response,3000);
        expect(response.body.user).to.deep.include({
            avatar: "http://placeimg.com/640/480/animals",
            email: randomEmail,
            userName: randomUserName,
        });
        
        console.log(randomEmail)
    });

    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await users.getAllUsers();

         checkStatusCode(response, 200);
         checkResponseTime(response,3000);
         expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
         expect(response.body).to.be.jsonSchema(schemas.schema_allUsers); 
        
        
    });

    it(`Login with valid credentialls`, async () => {
        let response = await auth.login(randomEmail, randonPassword);
        
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
        expect(response.body.user).to.deep.include({
            id: userId,
            avatar: "http://placeimg.com/640/480/animals",
            email: randomEmail,
            userName: randomUserName,
        });
        
    });

    it(`should return correct details of the currect user`, async () => {
        let response = await users.getCurrentUser(accessToken);
        userDataBeforeUpdate = response.body;

        expect(response.body).to.deep.include({
            id: userId,
            avatar: "http://placeimg.com/640/480/animals",
            email: randomEmail,
            userName: randomUserName,
        });
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
        
        
    });

    it(`should update username using valid data`, async () => {
        
        userDataToUpdate = {
            id: userDataBeforeUpdate.id,
            avatar: userDataBeforeUpdate.avatar,
            email: userDataBeforeUpdate.email,
            userName:"Yelyzaveta456",
        };

        let response = await users.updateUser(userDataToUpdate, accessToken);
        checkStatusCode(response, 204);
        checkResponseTime(response,3000);
        
    });

    it(`should return correct details of the updated user`, async () => {
        let response = await users.getCurrentUser(accessToken);
        userDataToUpdate = response.body;

        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
        expect(response.body).to.deep.include(userDataToUpdate)
    });

    it(`should return correct user details by id after updating`, async () => {
        let response = await users.getUserById(userDataBeforeUpdate.id);
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
        expect(response.body).to.be.deep.equal(userDataToUpdate, "User details is correct");
    });

    it(`should delete user by id `, async () => {
        let response1 = await auth.login(randomEmail, randonPassword);
        let response = await users.deleteUserById(userId, accessToken );

        checkStatusCode(response, 204);
        checkResponseTime(response,3000);
        
    });
})