import { expect } from "chai";
import { faker } from '@faker-js/faker';
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const auth = new AuthController();
const users = new UsersController();
const post = new PostsController()
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe("Happy path for posts", () => {

    let accessToken: string;
    let newUserId: number;
    let addComentData: object;
    let postsId: number;
    let createPost: object;
    let addLikeData: object;
    let newPostId: number;
    const randomEmail = faker.internet.email();
    const randomUserName = faker.internet.userName()
    const randonPassword = faker.internet.password()

    before(`Register and get the token`, async () => {
        let response = await auth.register(0, "http://placeimg.com/940/480/animals", randomEmail, randomUserName, randonPassword);
        checkStatusCode(response, 201);
        checkResponseTime(response,3000);
        expect(response.body.user).to.deep.include({
            avatar: "http://placeimg.com/940/480/animals",
            email: randomEmail,
            userName: randomUserName,
        });

        accessToken = response.body.token.accessToken.token;
        newUserId = response.body.user.id

    });


    it(`1Login with valid credentialls`, async () => {
        let response = await auth.login(randomEmail, randonPassword);
        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
        expect(response.body.user).to.deep.include({
            avatar: "http://placeimg.com/940/480/animals",
            email: randomEmail,
            userName: randomUserName,
        });
        
    });

    it(`2should return 200 status code and all users when getting all posts`, async () => {
        let response = await post.getAllPosts();
postsId = response.body[0].id
         checkStatusCode(response, 200);
         checkResponseTime(response,3000);
         expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
         expect(response.body).to.be.jsonSchema(schemas.schema_allUsers); 
        

        });
    
        it(`3should add new comment to post`, async () => {
            addComentData = {
                    authorId: newUserId,
                    postId: postsId,
                    body: "tryjkopo[p;poiuytr"
            };
            let response = await post.createComentToPost(addComentData, accessToken);
            checkStatusCode(response, 200);
            checkResponseTime(response,3000);
        });
    
        it(`3should create new post`, async () => {
            createPost = {
                    authorId: newUserId,
                    previewImage: "http://placeimg.com/640/480/animals",
                    body: "It'll be a very interesting post"
            };
            let response = await post.createNewPost(createPost, accessToken);
            newPostId = response.body.id
            checkStatusCode(response, 200);
            checkResponseTime(response,3000);
           
            
        });

        it(`4should add like to post`, async () => {
            addLikeData = {
                entityId: newPostId,
                isLike: true,
                userId: newUserId
            };
            let response = await post.addLikeToPost(addLikeData, accessToken);
            checkStatusCode(response, 200);
            checkResponseTime(response,3000);
            
        });
    
    
    
         after(`should delete user by id `, async () => {
        let response = await users.deleteUserById(newUserId, accessToken );
        checkStatusCode(response, 204);
        checkResponseTime(response,3000);
        
    });
})
